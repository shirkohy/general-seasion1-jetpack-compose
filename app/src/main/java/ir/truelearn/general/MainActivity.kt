package ir.truelearn.general


import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.*
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Animatable
import androidx.compose.animation.animateColor
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.*
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.ContentAlpha.medium
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.Menu
import androidx.compose.material.icons.twotone.Menu
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.layout.AlignmentLine
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.*
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.google.accompanist.insets.ProvideWindowInsets
import ir.truelearn.general.screens.CookingScreen
import ir.truelearn.general.ui.theme.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import me.nikhilchaudhari.library.neumorphic
import me.nikhilchaudhari.library.shapes.Punched
import kotlin.math.roundToInt
import kotlin.system.exitProcess


class MainActivity : ComponentActivity() {

    @OptIn(ExperimentalMaterialApi::class, ExperimentalMotionApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {


            Surface(color = Color.White) {
                CookingScreen()
            }


            /* Surface(color = Color.White) {
                 CollapsableToolbar()
             }*/


            /* val scaffoldState = rememberScaffoldState()
             val scope = rememberCoroutineScope()
             val modalState = rememberModalBottomSheetState(ModalBottomSheetValue.Hidden)
             Scaffold(
                 scaffoldState = scaffoldState,
                 topBar = {
                     TopAppBarView()
                 },
                 bottomBar = {
                     BottomAppBarView()
                 },
                 drawerContent = {
                     Text(text = "Hello Drawer!")
                 },
                 floatingActionButton = {
                     FloatingActionButton(
                         onClick = {
                             scope.launch {
                                 //scaffoldState.snackbarHostState.showSnackbar("Hello")
                                 modalState.show()
                             }
                         }) {
                         Text(text = "Hi")
                     }
                 },
                 content = {

                     GeneralTheme() {
                         Surface(modifier = Modifier.fillMaxSize()) {
                             MaterialTheme.shapes.medium
                             Column() {
                                 Button(
                                     onClick = {
                                         CustomThemeManager.customTheme =
                                             if (CustomThemeManager.customTheme == CustomTheme.DARK) {
                                                 CustomTheme.LIGHT
                                             } else {
                                                 CustomTheme.DARK
                                             }
                                     },
                                     colors = ButtonDefaults.buttonColors(
                                         backgroundColor = CustomThemeManager.colors.buttonBackgroundColor,
                                         contentColor = CustomThemeManager.colors.buttonTextColor
                                     )
                                 ) {
                                     Text(text = "Change My Theme...")
                                 }

                                 Spacer(modifier = Modifier.height(20.dp))

                                 Text(
                                     text = "Current theme is : ${
                                         if (CustomThemeManager.isSystemInDarkTheme()) {
                                             "Dark"
                                         } else {
                                             "Light"
                                         }
                                     }"
                                 )
                             }


                         }
                     }


                 }
             )*/


        }


    }

}



@Composable
fun CanvasDraw(){
    Canvas(modifier = Modifier.fillMaxSize()){

        drawLine(
            Color.Red,
            Offset(20f , 0f),
            Offset(200f , 200f),
            strokeWidth = 20f
        )

        drawArc(
            Color.Green,
            0f ,
            90f,
            useCenter = true,
            size = Size(300f , 300f),
            topLeft = Offset(400f , 50f)
        )
        drawCircle(
            Color.Blue,
            center = Offset(200f, 200f),
            radius = 60f
        )

        drawRect(
            Color.Gray,
            topLeft = Offset(250f,0f),
            size = Size(400f , 100f)
        )
    }
}

@Composable
fun InstagramLogoCanvas(){
    val instaColor = listOf(Color.Yellow , Color.Red , Color.Magenta)
    Canvas(
        modifier = Modifier
            .size(100.dp)
            .padding(16.dp)){

        drawRoundRect(
            brush = Brush.linearGradient(colors = instaColor),
            cornerRadius = CornerRadius(60f , 60f),
            style = Stroke(width = 15f , cap = StrokeCap.Round)
        )
        drawCircle(
            brush = Brush.linearGradient(colors = instaColor),
            radius = 45f,
            style = Stroke(width = 15f , cap = StrokeCap.Round),
            center = Offset(this.size.width*0.50f,this.size.height*0.50f)
        )
        drawCircle(
            brush = Brush.linearGradient(colors = instaColor),
            radius = 12f,
            center = Offset(this.size.width*0.80f,this.size.height*0.20f)
            )
    }
}


@Composable
fun ScrollableContent() {
    LazyColumn(
        modifier = Modifier.padding(bottom = 8.dp)
    ) {
        items(25) {
            WalletItem(
                name = "Bitcoin${it}",
                img = painterResource(id = R.drawable.btc),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
                    .clickable(
                        onClick = {

                        },
                        interactionSource = CreateMutableInteractionSource(),
                        indication = CreateIndication(color = Color.Red)
                    )
            )
        }
    }
}


@OptIn(ExperimentalMotionApi::class)
@Composable
fun MotionLayoutView(progress: Float, scrollableBody: @Composable () -> Unit) {
    val context = LocalContext.current
    val motionScene = remember {
        context.resources
            .openRawResource(R.raw.motion_scene)
            .readBytes()
            .decodeToString()
    }



    MotionLayout(
        motionScene = MotionScene(content = motionScene),
        progress = progress,
        modifier = Modifier.fillMaxWidth()

    ) {
        val properties = motionProperties(id = "title_txt")


        Image(
            painter = painterResource(id = R.drawable.my_pic),
            contentDescription = "",
            modifier = Modifier
                .layoutId("my_image")
        )

        Icon(
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = "",
            modifier = Modifier
                .layoutId("back_icon")
        )

        Text(
            text = "TrueLearn.ir",
            fontWeight = FontWeight.Bold,
            color = properties.value.color("text_color"),
            fontSize = 16.sp,
            modifier = Modifier.layoutId("title_txt")
        )

        Icon(
            imageVector = Icons.Filled.Share,
            contentDescription = "",
            tint = properties.value.color("text_color"),
            modifier = Modifier
                .layoutId("share_icon")
        )

        Box(modifier = Modifier.layoutId("content")) {
            scrollableBody()
        }


    }

}


@Composable
fun AnimateColor() {
    var isNeedColorChange by remember {
        mutableStateOf(false)
    }
    val startColor = Color.Red
    val endColor = Color.Blue
    val backgroundColor by animateColorAsState(
        targetValue = if (isNeedColorChange) endColor else startColor,
        animationSpec = tween(
            durationMillis = 5000,
            delayMillis = 100
        )
    )
    Column {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
                .background(backgroundColor)
        )
        Button(onClick = {
            isNeedColorChange = !isNeedColorChange
        }) {
            Text(text = "change color")
        }
    }
}


@Composable
fun RotateAnimation() {
    var isRotate by remember {
        mutableStateOf(false)
    }
    val rotateAngle by animateFloatAsState(
        targetValue = if (isRotate) 360F else 0f,
        animationSpec = tween(durationMillis = 3000)
    )
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.btc),
            contentDescription = "",
            modifier = Modifier
                .rotate(rotateAngle)
                .size(150.dp)
        )
        Button(onClick = {
            isRotate = !isRotate
        }) {
            Text(text = "Rotate")
        }
    }
}


@Composable
fun InfiniteAnimation() {
    val infiniteTransition = rememberInfiniteTransition()
    val size by infiniteTransition.animateFloat(
        initialValue = 100.0f,
        targetValue = 150.0f,
        animationSpec = infiniteRepeatable(
            animation = tween(800, 100),
            repeatMode = RepeatMode.Reverse
        )
    )
    Image(
        painter = painterResource(id = R.drawable.btc),
        contentDescription = "",
        modifier = Modifier
            .size(size.dp)
    )
}

@Composable
fun AnimateDp() {
    var sizeState by remember {
        mutableStateOf(100.dp)
    }
    val size by animateDpAsState(
        targetValue = sizeState,
        tween(
            durationMillis = 3000,
            delayMillis = 5000
        )
    )
    Image(
        painter = painterResource(id = R.drawable.btc),
        contentDescription = "",
        modifier = Modifier
            .size(size)
    )

    Button(onClick = {
        sizeState += 50.dp
    }) {
        Text(text = "Increase Size")
    }

}



@Composable
fun WalletItem(name: String, img: Painter, modifier: Modifier) {
    Card(
        modifier = modifier,
        elevation = 8.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {

            Column(
                modifier = Modifier
                    .padding(8.dp)
                    .weight(0.15f),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CircleImageView(
                    painter = img,
                    size = 32.dp
                )
            }
            Column(
                modifier = Modifier
                    .padding(8.dp)
                    .weight(0.475f)
            ) {
                Text(
                    text = name,
                    modifier = Modifier.padding(bottom = 8.dp),
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp
                )
                Text(
                    text = "BTC/USD",
                    modifier = Modifier.padding(bottom = 8.dp),
                    color = Color.Gray
                )
            }
            Column(
                modifier = Modifier
                    .padding(top = 8.dp, bottom = 8.dp, end = 16.dp)
                    .weight(0.475f),
                horizontalAlignment = Alignment.End
            ) {
                Text(
                    text = "BTC 0.01123",
                    modifier = Modifier.padding(bottom = 8.dp),
                    fontWeight = FontWeight.Bold,
                    fontSize = 17.sp
                )
                Text(
                    text = "$895",
                    modifier = Modifier.padding(bottom = 8.dp),
                    fontWeight = FontWeight.Bold,
                    color = Color(0xFF299C0D)
                )
            }

        }
    }
}


val openDialogState = mutableStateOf(false)

@Composable
fun ExitAlertDialog() {
    MaterialTheme {
        var openDialog by remember {
            openDialogState
        }

        if (openDialog) {
            AlertDialog(
                onDismissRequest = {
                    openDialog = false
                },
                title = {
                    Text(text = "Exit app")
                },
                text = {
                    Text(text = "are you sure to exit app?")
                },
                confirmButton = {
                    Button(
                        onClick = {
                            openDialog = false
                            exitProcess(0)
                        }) {
                        Text(text = "OK")
                    }
                },
                dismissButton = {
                    Button(
                        onClick = {
                            openDialog = false
                        }) {
                        Text(text = "cancel")
                    }
                }
            )
        }


    }
}


@Composable
fun BottomAppBarView() {
    BottomAppBar(
        backgroundColor = Purple500,
    ) {
        BottomNavigationItem(
            selected = true,
            onClick = { /*TODO*/ },
            label = {
                Text(text = "Home")
            },
            icon = {
                Icon(Icons.Filled.Home, contentDescription = "")
            }
        )
        BottomNavigationItem(
            selected = false,
            onClick = { /*TODO*/ },
            label = {
                Text(text = "Setting")
            },
            icon = {
                Icon(Icons.Filled.Settings, contentDescription = "")
            }
        )
        BottomNavigationItem(
            selected = false,
            onClick = { /*TODO*/ },
            label = {
                Text(text = "Profile")
            },
            icon = {
                Icon(Icons.Filled.Info, contentDescription = "")
            }
        )
        BottomNavigationItem(
            selected = false,
            onClick = { /*TODO*/ },
            label = {
                Text(text = "Menu")
            },
            icon = {
                Icon(Icons.Filled.Menu, contentDescription = "")
            }
        )
    }
}

@Composable
fun TopAppBarView() {
    Column() {
        TopAppBar(
            elevation = 10.dp,
            title = {
                Text(text = "Truelearn Course")
            },
            backgroundColor = Purple500,
            actions = {
                IconButton(onClick = { /*TODO*/ }) {
                    Icon(Icons.Filled.Star, null)
                }

                IconButton(onClick = { /*TODO*/ }) {
                    Icon(Icons.Filled.Menu, null)
                }

            },
            navigationIcon = {
                IconButton(
                    onClick = {
                        openDialogState.value = true
                    }) {
                    Icon(Icons.Filled.ArrowBack, null)
                }
            }
        )
    }
}


@Composable
fun MyEditText() {

    var textState by remember {
        mutableStateOf(TextFieldValue())
    }

    Column(
        modifier = Modifier
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        OutlinedTextField(
            value = textState,
            onValueChange = {
                textState = it
            })
        Text(text = "your text is : ${textState.text}")


        Row() {
            OutlinedButton(
                modifier = Modifier
                    .padding(16.dp),
                onClick = {
                    Log.e("3636", textState.text)
                }) {
                Text(text = "Register")
            }

            Button(
                modifier = Modifier
                    .padding(16.dp),
                onClick = {
                    Log.e("3636", textState.text)
                }) {
                Text(text = "Login")
            }
        }


        TextButton(
            modifier = Modifier
                .padding(16.dp),
            onClick = {
                Log.e("3636", textState.text)
            }) {
            Text(text = "Forget Password?")
        }


    }

}

@Composable
fun SeekBarView() {
    var sliderPosition by remember {
        mutableStateOf(0f)
    }
    Column() {
        Slider(
            value = sliderPosition,
            onValueChange = {
                sliderPosition = it
            }
        )
        Text(text = (sliderPosition * 100).roundToInt().toString())
    }

}


@Composable
fun SwitchView() {
    var checkState by remember {
        mutableStateOf(false)
    }
    Switch(
        checked = checkState,
        onCheckedChange = {
            checkState = it
        }
    )

}

@Composable
fun RadioButtonView() {

    val radioOptions = listOf("A", "B", "C", "D")

    val (selectedOptions, onOptionSelected) = remember {
        mutableStateOf(radioOptions[0])
    }

    Row() {
        radioOptions.forEach { text ->
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .selectable(
                        selected = (text == selectedOptions),
                        onClick = {
                            onOptionSelected(text)
                            Log.e("3636", text)
                        }
                    )
            ) {
                RadioButton(
                    selected = (text == selectedOptions),
                    onClick = {
                        onOptionSelected(text)
                        Log.e("3636", text)
                    })
                Text(
                    text = text,
                    modifier = Modifier.padding(start = 12.dp)
                )
            }
        }
    }


}

@Composable
fun CheckBoxView() {
    var checkState by remember {
        mutableStateOf(true)
    }
    Checkbox(
        checked = checkState,
        onCheckedChange = {
            checkState = it
        })

}


@Composable
fun CircleImageView(painter: Painter, size: Dp) {
    Image(
        painter = painter,
        contentDescription = "circle image",
        modifier = Modifier
            .clip(CircleShape)
            .size(size)
//            .border(
//                width = 6.dp,
//                color = Color.Magenta,
//                shape = CircleShape
//            )
    )
}

@Composable
fun ShapeGenerator(shape: Shape, color: Color, width: Dp, height: Dp, modifier: Modifier) {
    Column(modifier) {
        Box(
            modifier = Modifier
                .clip(shape)
                .size(width, height)
                .background(color)
        )
    }
}


@Composable
fun BoxCard(title: String, price: String, image: Painter) {
    Card(
        modifier = Modifier
            .padding(8.dp),
        elevation = 10.dp,
        shape = RoundedCornerShape(20.dp)
    ) {
        Box(
            modifier = Modifier
                .height(180.dp)
                .width(150.dp)
        ) {

            Image(
                painter = image,
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier.fillMaxSize()
            )

            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                Color.Black
                            )
                        )
                    )
            )



            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(8.dp),
                contentAlignment = Alignment.BottomStart
            ) {
                Text(
                    text = title,
                    fontWeight = FontWeight.Bold,
                    fontSize = 20.sp,
                    color = Color.White
                )
            }


        }
    }
}

@Composable
fun CardView(title: String, price: String, image: Painter) {
    Card(
        modifier = Modifier
            .width(170.dp)
            .padding(8.dp),
        elevation = 10.dp,
        shape = RoundedCornerShape(20.dp)
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Image(
                painter = image,
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(180.dp)
            )

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxWidth()
            ) {

                Text(
                    text = title,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
                Text(
                    text = price,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp,
                    color = Color.Red
                )
            }

        }
    }
}


@Composable
fun FoodItem(title: String, desc: String, image: Painter, price: String) {
    Column() {
        Image(
            painter = image,
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
        )

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = title,
                fontWeight = FontWeight.Bold,
                fontSize = 24.sp,
                modifier = Modifier.padding(start = 8.dp, top = 4.dp)
            )
            Text(
                text = price,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                color = Color.Magenta,
                modifier = Modifier.padding(8.dp)
            )
        }

        Text(
            text = desc,
            fontSize = 20.sp,
            modifier = Modifier.padding(start = 8.dp, top = 4.dp, bottom = 36.dp)
        )
    }
}


@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    GeneralTheme {
        CircleImageView(
            painterResource(id = R.drawable.flower),
            size = 100.dp
        )
    }
}