package ir.truelearn.general

import android.app.Activity
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import ir.truelearn.general.databinding.ActivityMapBinding
import org.osmdroid.config.Configuration.getInstance
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView


class MapActivity : ComponentActivity() {

    private lateinit var binding: ActivityMapBinding


    @ExperimentalMaterialApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        val view = binding.root

        getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this))
        setContentView(view)

        binding.map.setTileSource(TileSourceFactory.MAPNIK);

        val mapController = binding.map.controller
        mapController.setZoom(15.0)
        val startPoint = GeoPoint(35.7658175264987, 51.46884817684654);
        mapController.setCenter(startPoint);


        binding.composeView.apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {

                Column() {
                    WalletItem(
                        img = painterResource(id = R.drawable.btc),
                        name = "Bitcoin",
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 16.dp, end = 16.dp, top = 8.dp, bottom = 8.dp)
                            .clickable(
                                onClick = {
                                    Toast
                                        .makeText(context, "Clicked!", Toast.LENGTH_LONG)
                                        .show()
                                },
                                interactionSource = CreateMutableInteractionSource(),
                                indication = CreateIndication(color = Color.Red)
                            )
                    )
                }

            }
        }

    }
}

@Composable
fun CreateMutableInteractionSource(): MutableInteractionSource = remember {
    MutableInteractionSource()
}

@Composable
fun CreateIndication(bounded: Boolean = true, color: Color = Color.Gray) =
    rememberRipple(bounded = bounded, color = color)