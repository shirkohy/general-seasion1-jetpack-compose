package ir.truelearn.general.ui.theme

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import androidx.compose.ui.text.font.FontWeight.Companion.Medium
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.min
import androidx.compose.ui.unit.sp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.statusBarsPadding
import ir.truelearn.general.R
import kotlin.math.max
import kotlin.math.min

val AppBarCollapsedHeight = 56.dp
val AppBarExpendedHeight = 400.dp

@Composable
fun CookingScreens() {

    val scrollState = rememberLazyListState()

    Box {
        Content(scrollState)
        ParallaxToolbar(scrollState)
    }
}

@Composable
fun ParallaxToolbar(scrollState: LazyListState) {
    val imageHeight = AppBarExpendedHeight - AppBarCollapsedHeight

    val maxOffset =
        with(LocalDensity.current) { imageHeight.roundToPx() } - LocalWindowInsets.current.systemBars.layoutInsets.top

    val offset = min(scrollState.firstVisibleItemScrollOffset, maxOffset)

    val offsetProgress = max(0f, offset * 3f - 2f * maxOffset) / maxOffset

    TopAppBar(
        contentPadding = PaddingValues(),
        backgroundColor = Color.White,
        modifier = Modifier
            .height(
                AppBarExpendedHeight
            )
            .offset { IntOffset(x = 0, y = -offset) },
        elevation = if (offset == maxOffset) 4.dp else 0.dp
    ) {
        Column {
            Box(
                Modifier
                    .height(imageHeight)
                    .graphicsLayer {
                        alpha = 1f - offsetProgress
                    })
            {
                Image(
                    painter = painterResource(id = R.drawable.strawberry_pie_1),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.fillMaxSize()
                )

                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            Brush.verticalGradient(
                                colorStops = arrayOf(
                                    Pair(0.4f, Color.Transparent),
                                    Pair(1f, Color.White)
                                )
                            )
                        )
                )

                Row(
                    modifier = Modifier
                        .fillMaxHeight()
                        .padding(horizontal = 16.dp, vertical = 8.dp),
                    verticalAlignment = Alignment.Bottom
                ) {
                    Text(
                        "Dessert",
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier
                            .clip(Shapes.small)
                            .background(LightGray)
                            .padding(vertical = 6.dp, horizontal = 16.dp)
                    )
                }
            }


            Column(
                Modifier
                    .fillMaxWidth()
                    .height(AppBarCollapsedHeight),
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    "Strawberry cake",
                    fontSize = 26.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .padding(horizontal = (16 + 28 * offsetProgress).dp)
                        .scale(1f - 0.25f * offsetProgress)
                )

            }
        }
    }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .statusBarsPadding()
            .height(AppBarCollapsedHeight)
            .padding(horizontal = 16.dp)
    ) {
        CircularButton(R.drawable.ic_arrow_back)
        CircularButton(R.drawable.ic_favorite)
    }
}

@Composable
fun CircularButton(
    @DrawableRes iconRes: Int,
    color: Color = Color.Gray,
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    onClick: () -> Unit = {}
) {
    Button(
        onClick = onClick,
        contentPadding = PaddingValues(),
        shape = Shapes.small,
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.White, contentColor = color),
        elevation = elevation,
        modifier = Modifier
            .width(38.dp)
            .height(38.dp)
    ) {
        Icon(painterResource(id = iconRes), null)
    }
}


@Composable
fun Content(scrollState: LazyListState) {
    LazyColumn(
        contentPadding = PaddingValues(top = AppBarExpendedHeight),
        state = scrollState) {
        item {
            BasicInfo()
            Description()
            ServingCalculator()
            ShoppingListButton()

            SimilarFoods()
            SimilarRecipes()

        }
    }
}


@Composable
fun SimilarRecipes() {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(text = "Similar Recipes", fontWeight = Bold)
            Text("You can also read these...", color = DarkGray)
        }
        Button(
            onClick = { /*TODO*/ }, elevation = null, colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Transparent, contentColor = Pink
            )
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("See all")
                Icon(
                    painter = painterResource(id = R.drawable.ic_arrow_right),
                    contentDescription = null
                )
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp),
    ) {
        RecipeItem(
            name = "Cheesecake",
            category = "Dessert",
            exp = "Beginner",
            baseColor = Color(0xFFcbe8e0),
            expColor = Color(0xFF8cd694),
            img = painterResource(id = R.drawable.cheesecake)
        )

        RecipeItem(
            name = "Cupcake",
            category = "Dessert",
            exp = "2+ Year's Experience",
            baseColor = Color(0xFFe8d0ff),
            expColor = Color(0xFF7c89ff),
            img = painterResource(id = R.drawable.cupcake)
        )

        RecipeItem(
            name = "Berry Cake",
            category = "Breakfast",
            exp = "Beginner",
            baseColor = Color(0xFFfae9d4),
            expColor = Color(0xFF8cd694),
            img = painterResource(id = R.drawable.berrycake)
        )




    }
}

@Composable
fun RecipeItem(
    img:Painter ,
    name: String ,
    category:String ,
    exp:String ,
    baseColor:Color,
    expColor:Color
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(120.dp)
            .padding(bottom = 16.dp)
    ) {
        Column(
            modifier = Modifier
                .weight(0.30f)
                .fillMaxSize()

        ) {
            Column(
                modifier = Modifier
                    .width(120.dp)
                    .height(120.dp)
                    .clip(CircleShape)
                    .background(baseColor),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    modifier = Modifier.padding(8.dp),
                    painter = img,
                    contentDescription = ""
                )
            }
        }

        Column(
            modifier = Modifier
                .weight(0.6f)
                .fillMaxHeight()
                .padding(16.dp)
        ) {
            Text(
                modifier = Modifier
                    .weight(0.33f),
                text = name,
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp,
                textAlign = TextAlign.Center
            )
            Text(
                modifier = Modifier
                    .weight(0.33f),
                text = category,
                fontWeight = FontWeight.Bold,
                color = Color.Gray,
                fontSize = 14.sp,
                textAlign = TextAlign.Center
            )
            Row(
                modifier = Modifier
                    .weight(0.33f),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier
                        .width(20.dp)
                        .height(20.dp)
                        .clip(RoundedCornerShape(8.dp))
                        .background(expColor)
                ){}


                Text(
                    modifier = Modifier.padding(start = 8.dp),
                    text = exp,
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp,
                )

            }
        }

        Column(
            modifier = Modifier
                .weight(0.1f)
                .padding(top = 16.dp)
                .fillMaxHeight()

        ) {
            Icon(imageVector = Icons.Filled.KeyboardArrowRight, null)
        }
    }
}

@Composable
fun SimilarFoods() {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(text = "Similar Foods", fontWeight = Bold)
            Text("You may like these...", color = DarkGray)
        }
        Button(
            onClick = { /*TODO*/ }, elevation = null, colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Transparent, contentColor = Pink
            )
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("See all")
                Icon(
                    painter = painterResource(id = R.drawable.ic_arrow_right),
                    contentDescription = null
                )
            }
        }
    }


    Row(
        modifier = Modifier
            .horizontalScroll(rememberScrollState())
            .padding(horizontal = 16.dp, vertical = 8.dp),
    ) {
        FastFoodItem(
            img = painterResource(id = R.drawable.hot_dog),
            name = "Hot Dog",
            desc = "Fast Foods",
            price = "45$"
        )
        FastFoodItem(
            img = painterResource(id = R.drawable.doughnut),
            name = "Doughnut",
            desc = "Dessert",
            price = "32$"
        )
        FastFoodItem(
            img = painterResource(id = R.drawable.hamburger),
            name = "Hamburger",
            desc = "Fast Foods",
            price = "56$"
        )
        FastFoodItem(
            img = painterResource(id = R.drawable.apple_pie),
            name = "Apple pie",
            desc = "Cookies",
            price = "26$"
        )

    }
}

@Composable
fun FastFoodItem(
    img: Painter,
    name: String,
    desc: String,
    price: String
) {
    Box(
        modifier = Modifier
            .width(170.dp)
            .height(250.dp)
            .padding(8.dp),
        contentAlignment = Alignment.BottomCenter
    ) {

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
                .clip(RoundedCornerShape(16.dp))
                .background(LightGray)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 75.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    fontWeight = FontWeight.Bold,
                    fontSize = 16.sp,
                    text = name
                )
                Text(
                    color = Color.Gray,
                    text = desc,
                    modifier = Modifier.padding(top = 5.dp)
                )

                Row(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = 14.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Text(
                        text = price,
                        fontWeight = FontWeight.ExtraBold,
                        fontSize = 16.sp,
                        color = Pink
                    )

                    Button(
                        contentPadding = PaddingValues(),
                        shape = RoundedCornerShape(topEnd = 16.dp, topStart = 16.dp),
                        modifier = Modifier
                            .padding(top = 5.dp)
                            .fillMaxHeight()
                            .width(38.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Pink,
                            contentColor = Color.White
                        ),
                        onClick = {},

                        ) {
                        Icon(
                            painterResource(id = R.drawable.ic_plus),
                            null
                        )
                    }

                }
            }

        }

        Box(modifier = Modifier.fillMaxSize())
        {
            Image(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.6f),
                painter = img,
                contentDescription = ""
            )
        }


    }
}

@Composable
fun ShoppingListButton() {
    Column(Modifier.padding(16.dp)) {
        Button(
            onClick = { /*TODO*/ },
            elevation = null,
            shape = Shapes.small,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Pink,
                contentColor = Color.Black
            ), modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(
                color = Color.White,
                text = "Add to shopping list",
                modifier = Modifier
                    .padding(8.dp)

            )
        }
    }


}


@Composable
fun ServingCalculator() {
    var value by remember { mutableStateOf(6) }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .clip(Shapes.medium)
            .background(LightGray)
            .padding(horizontal = 16.dp)
    ) {

        Text(text = "Serving", Modifier.weight(1f), fontWeight = Medium)
        CircularButton(iconRes = R.drawable.ic_minus, elevation = null, color = Pink) { value-- }
        Text(text = "$value", Modifier.padding(16.dp), fontWeight = Medium)
        CircularButton(iconRes = R.drawable.ic_plus, elevation = null, color = Pink) { value++ }
    }
}

@Composable
fun Description() {
    Text(
        text = "This Dessert is very tasty and not very hard to prepare. and please pay attention that you can replace strawberry with any other fruit you like",
        fontWeight = Medium,
        modifier = Modifier.padding(horizontal = 16.dp, vertical = 16.dp)
    )
}

@Composable
fun BasicInfo() {
    Row(
        horizontalArrangement = Arrangement.SpaceEvenly,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
    ) {
        InfoColumn(R.drawable.ic_clock, "60 min")
        InfoColumn(R.drawable.ic_flame, "735 kcal")
        InfoColumn(R.drawable.ic_star, "4.8")
    }
}

@Composable
fun InfoColumn(@DrawableRes iconResouce: Int, text: String) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Icon(
            painter = painterResource(id = iconResouce),
            contentDescription = null,
            tint = Pink,
            modifier = Modifier.height(24.dp)
        )
        Text(text = text, fontWeight = Bold)
    }
}


